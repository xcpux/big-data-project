from google.cloud import bigquery
from pyspark.sql import SQLContext
import pprint

try:
    spark
except NameError:
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.appName("final-project").getOrCreate()

sc = spark.SparkContext()

# client = bigquery.Client(project='winter-berm-237719')
# # patentsview
# dataset_ref = client.dataset('patentsview', project='patents-public-data')
# dataset = client.get_dataset(dataset_ref)

# print(dataset_ref)
# print(type(dataset_ref))

# print([x.table_id for x in client.list_tables(dataset)])
conf = {
    'mapred.bq.input.project.id': 'patents-public-data',
    'mapred.bq.input.dataset.id': 'patentsview',
    'mapred.bq.input.table.id': 'patent'
}

table_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=conf
)

pprint(table_data.take(10))
