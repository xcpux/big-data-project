from google.cloud import bigquery
from pyspark.sql import SQLContext
import json
from pyspark import SparkContext
import pprint
import csv
import numpy as np

def join_patents_and_figures(sc,  quarterly=False):
    conf = {
        'mapred.bq.input.project.id': 'patents-public-data',
        'mapred.bq.input.dataset.id': 'patentsview',
        'mapred.bq.input.table.id': 'figures'
    }

    conf2 = {
        'mapred.bq.input.project.id': 'patents-public-data',
        'mapred.bq.input.dataset.id': 'patentsview',
        'mapred.bq.input.table.id': 'patent'
    }

    figures_data = sc.newAPIHadoopRDD(
        'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
        'org.apache.hadoop.io.LongWritable',
        'com.google.gson.JsonObject',
        conf=conf
    )

    patent_data = sc.newAPIHadoopRDD(
        'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
        'org.apache.hadoop.io.LongWritable',
        'com.google.gson.JsonObject',
        conf=conf2
    )

    patent_data = patent_data.map(lambda rdd: json.loads(rdd[1])).map(lambda rdd:(rdd["id"],(rdd["date"])))
    figures_data = figures_data.map(lambda rdd: json.loads(rdd[1])).map(lambda rdd: (rdd["patent_id"],
                                                                                     (rdd["num_figures"], rdd["num_sheets"])))
    if quarterly:
        joined_data = patent_data.join(figures_data).map(lambda combined: (combined[1][0],combined[1][1][0], combined[1][1][1])).map(
            lambda rdd: (date_to_quarter(rdd[0]), (rdd[1], rdd[2], 1))
        ).reduceByKey(lambda v1, v2: (int(v1[0]) + int(v2[0]), int(v1[1]) + int(v2[1]), int(v1[2]) + int(v2[2]))).map(lambda rdd: (rdd[0], (rdd[1][0]/rdd[1][2], rdd[1][1]/rdd[1][2], rdd[1][2])))
    else:
        joined_data = patent_data.join(figures_data).map(
            lambda combined: (combined[1][0], combined[1][1][0], combined[1][1][1])).map(
            lambda rdd: (rdd[0][0:4], (rdd[1], rdd[2], 1))
        ).reduceByKey(lambda v1, v2: (int(v1[0]) + int(v2[0]), int(v1[1]) + int(v2[1]), int(v1[2]) + int(v2[2]))).map(
            lambda rdd: (rdd[0], (rdd[1][0] / rdd[1][2], rdd[1][1] / rdd[1][2], rdd[1][2])))
    return joined_data

def date_to_quarter(date):
    to_process = date[0:5]
    quarter = int(date[5:7])
    return to_process + str((quarter - 1)//3)


def linear_regression_on_pat_freq(sc):
    GDP_PATH = 'hdfs:///user/kchiguichon/GDP.csv'
    gdp_rdd = sc.textFile(GDP_PATH).mapPartitions(lambda x: csv.reader(x))
    gdp_header = gdp_rdd.first()
    gdp_rdd = gdp_rdd.filter(lambda x: x != gdp_header)
    gdp_mapped_rdd = gdp_rdd.map(lambda x: (x[0][:5] + str(((int(x[0][5:7]) - 1) // 3)), x[1]))
    figures_frequency = join_patents_and_figures(sc, True)
    joined_rdd = figures_frequency.join(gdp_mapped_rdd).map(lambda rdd: ("A", (rdd[0], rdd[1]))).groupByKey().map(lambda rdd: list(rdd[1])).map(
        lambda rdd: (np.array([item[1][0] for item in rdd]).astype(np.float), np.array([item[1][1] for item in rdd]).astype(np.float))).map(lambda rdd: multiple_linear_regression(rdd[0], rdd[1]))
    return joined_rdd

def multiple_linear_regression(X, Y):
    X, Y = (X - np.mean(X)) / np.std(X), (Y - np.mean(Y)) / np.std(Y)
    X = np.c_[np.ones(len(X)), X]
    Beta = inv(X.T @ X) @ X.T @ Y
    return Beta



sc = SparkContext.getOrCreate()
print(linear_regression_on_pat_freq(sc).take(1))
