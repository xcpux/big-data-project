from patents_categories_per_year import compute_reg
import json
import csv

GDP_PATH = 'hdfs:///user/kchiguichon/GDP.csv'

def patent_to_wipo(sc):
    conf = {
        'mapred.bq.input.project.id': 'patents-public-data',
        'mapred.bq.input.dataset.id': 'patentsview',
        'mapred.bq.input.table.id': 'wipo'
    }

    conf2 = {
        'mapred.bq.input.project.id': 'patents-public-data',
        'mapred.bq.input.dataset.id': 'patentsview',
        'mapred.bq.input.table.id': 'wipo_field'
    }

    conf3 = {
        'mapred.bq.input.project.id': 'patents-public-data',
        'mapred.bq.input.dataset.id': 'patentsview',
        'mapred.bq.input.table.id': 'patent'
    }

    wipo_patents_data = sc.newAPIHadoopRDD(
        'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
        'org.apache.hadoop.io.LongWritable',
        'com.google.gson.JsonObject',
        conf=conf
    )

    wipo_category_data = sc.newAPIHadoopRDD(
        'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
        'org.apache.hadoop.io.LongWritable',
        'com.google.gson.JsonObject',
        conf=conf2
    )

    patent_data = sc.newAPIHadoopRDD(
        'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
        'org.apache.hadoop.io.LongWritable',
        'com.google.gson.JsonObject',
        conf=conf3
    )

    wipo_patents_data = wipo_patents_data.map(lambda x: json.loads(x[1]))
    wipo_category_data = wipo_category_data.map(lambda x: json.loads(x[1]))
    patent_data = patent_data.map(lambda x: json.loads(x[1]))
    category_data = wipo_category_data.map(lambda x: (x["id"],x["field_title"]))
    wipo_patents_data = wipo_patents_data.map(lambda x: (x["field_id"], x["patent_id"]))
    joined_wipo_date = category_data.join(wipo_patents_data).map(lambda x: x[1]).map(lambda x: (x[1], x[0]))
    patent_data = patent_data.map(lambda x: (x["id"], x["date"]))
    full_patent_data = patent_data.join(joined_wipo_date).map(lambda x: x[1]).map(lambda x: (1,json.dumps({"date": x[0], "field_title": x[1]})))
    gdp_rdd = sc.textFile(GDP_PATH).mapPartitions(lambda x: csv.reader(x))
    gdp_header = gdp_rdd.first()
    gdp_rdd = gdp_rdd.filter(lambda x: x != gdp_header)
    return compute_reg(full_patent_data, gdp_rdd, "field_title")


