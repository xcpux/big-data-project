from google.cloud import bigquery
from pyspark.sql import SQLContext
import json
from pyspark import SparkContext
import pprint
import csv
import numpy as np

def date_to_quarter(date):
    to_process = date[0:5]
    quarter = int(date[5:7])
    return to_process + str((quarter - 1)//3)

def citations_per_year(sc,  quarterly=False):
    conf = {
        'mapred.bq.input.project.id': 'patents-public-data',
        'mapred.bq.input.dataset.id': 'patentsview',
        'mapred.bq.input.table.id': 'figures'
    }

    citation_data = sc.newAPIHadoopRDD(
        'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
        'org.apache.hadoop.io.LongWritable',
        'com.google.gson.JsonObject',
        conf=conf
    )

    citation_data = citation_data.map(lambda rdd: json.loads(rdd[1])).map(lambda rdd:(rdd["date"][0:5], 1))
    if quarterly:
        citation_data.map(lambda rdd: date_to_quarter).reduceByKey(lambda v1, v2: v1 + v2)
        return citation_data
    else:
        citation_data.reduceByKey(lambda v1, v2: v1+v2)
    return citation_data
