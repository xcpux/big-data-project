from pyspark import SparkContext
import csv
import json
from subprocess import Popen, PIPE
import os.path

def expand_year_and_name(years, wdi_rdd):
    res = []
    key_name = wdi_rdd[2]
    year_data = wdi_rdd[4:-1]
    for i in range(len(years)):
        if (year_data[i] != ""):
            res.append((key_name, (years[i], year_data[i])))
    return res

def parse_wdi_data(sc, filepath):
    rdd = sc.textFile(filepath).mapPartitions(lambda x: csv.reader(x))
    header = rdd.first()
    years = sc.broadcast(header[4:-1])
    usa_rdd = rdd.filter(lambda x: x != header).filter(lambda x: x[1] == "USA")
    mapped_rdd = usa_rdd.filter(lambda x: len(["C" for i in x if i != ""]) - 4 >= (int(years.value[-1]) - int (years.value[0])) // 2)
    mapped_rdd = mapped_rdd.flatMap(lambda x: expand_year_and_name(years.value, x))
    return mapped_rdd

def write_keys(keys):
    with open("WDIkeys.txt", "w") as file:
        formatted_keys = [key + "\n" for key in keys]
        file.writelines(formatted_keys)

def getYFromIndicator(rdd, indicator_name):
    selected_rdd = rdd.filter(lambda x: x[0] == indicator_name).map(lambda x: (x[1][0], x[1][1]))
    return selected_rdd

if __name__ == "__main__":
    sc = SparkContext.getOrCreate()
    WDI_PATH = "hdfs:///user/jonapi10/WDIData.csv"
    wdi_data = parse_wdi_data(sc, WDI_PATH)
    if (not os.path.exists("WDIkeys.txt")):
        keys = wdi_data.map(lambda x: x[0]).distinct().collect()
        write_keys(keys)


