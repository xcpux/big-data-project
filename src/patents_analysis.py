### 
#   Group Members: 
#       Kenneth Chiguichon, Darwin Shen, Jonathan Yin
#   
#   Code Description:
#       This file contains the code to compute linear regression using various regularization techniques
#       for the patent data located under the google patents-public-data dataset.
# 
#   Data Frameworks:
#       1) Spark (map/reduce) was used for processing and analyzing the data. 
#       2) All data was distributed over Hadoop HDFS.
#       3) The data was imported from BigQuery and then converted to an RDD for Spark.
# 
#   Class Concepts:
#       1) Linear regression is performed in the compute_reg functions (3 of them). 
#       2) Regularization is done in the regression function. 
#           - Ridge was fully implemented by us.
#           - LASSO was taken from sklearn
#       3) Bonferroni Correction for the p values was applied for hypothesis testing.
# 
#   System Specs:
#       OS: Ubuntu
#       IAAS Provider: Google Cloud (DataProc)
#       Nodes: 5+1 (workers + master)
#       CPU(s) per Node: 4
#       RAM per Node: 16GB
#       Storage per Node: 100GB (HDD)
###

import json
import pprint
import numpy as np
import csv
import math
import io
from google.cloud import bigquery
from pyspark.sql import SQLContext
from numpy.linalg import inv
from scipy import stats
from sklearn.linear_model import Lasso

try:
    spark
except NameError:
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.appName("final-project").getOrCreate()

try:
    sc
except NameError:
    sc = spark.sparkContext

### Data configuration dictionaries for BigQuery

wipo_field_conf = {
    'mapred.bq.input.project.id': 'patents-public-data',
    'mapred.bq.input.dataset.id': 'patentsview',
    'mapred.bq.input.table.id': 'wipo'
}

wipo_patent_conf = {
    'mapred.bq.input.project.id': 'patents-public-data',
    'mapred.bq.input.dataset.id': 'patentsview',
    'mapred.bq.input.table.id': 'wipo_field'
}

patent_conf = {
    'mapred.bq.input.project.id': 'patents-public-data',
    'mapred.bq.input.dataset.id': 'patentsview',
    'mapred.bq.input.table.id': 'patent'
}

application_conf = {
    'mapred.bq.input.project.id': 'patents-public-data',
    'mapred.bq.input.dataset.id': 'patentsview',
    'mapred.bq.input.table.id': 'application'
}

foreign_citations_conf = {
    'mapred.bq.input.project.id': 'patents-public-data',
    'mapred.bq.input.dataset.id': 'patentsview',
    'mapred.bq.input.table.id': 'foreigncitation'
}

google_patents_publications_conf = {
    'mapred.bq.input.project.id': 'patents-public-data',
    'mapred.bq.input.dataset.id': 'patents',
    'mapred.bq.input.table.id': 'publications'
}

figures_conf = {
     'mapred.bq.input.project.id': 'patents-public-data',
     'mapred.bq.input.dataset.id': 'patentsview',
     'mapred.bq.input.table.id': 'figures'
}

### Loading data from BigQuery into RDD

patent_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=patent_conf
)

figures_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=figures_conf
)

wipo_patents_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=wipo_patent_conf
)

wipo_category_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=wipo_field_conf
)

application_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=application_conf
)

foreign_citations_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=foreign_citations_conf
)

google_patents_publications_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=google_patents_publications_conf
)

figures_data = sc.newAPIHadoopRDD(
    'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
    'org.apache.hadoop.io.LongWritable',
    'com.google.gson.JsonObject',
    conf=figures_conf
)

GDP_PATH = 'hdfs:///user/kchiguichon/GDP.csv'
gdp_rdd = sc.textFile(GDP_PATH).mapPartitions(lambda x: csv.reader(x))
gdp_header = gdp_rdd.first()
gdp_rdd = gdp_rdd.filter(lambda x: x != gdp_header)

# Compute P value for a given Beta given a two-tailed distribution.
def compute_P(X, Y, Beta, beta_index):
    degrees_freedom = len(X) - len(X[0])
    Y_predicted = np.matmul(X, Beta)
    RSS = np.sum((Y - Y_predicted)**2)
    X_col = X[:,beta_index]
    std_err = RSS / degrees_freedom
    denom = np.sum((X_col - X_col.mean())**2)
    std_err = np.sqrt(std_err/denom)
    t = Beta[beta_index]/std_err
    pval = 1 - stats.t.cdf(t, df=degrees_freedom)
    return pval * 2

# Computes Multiple linear regression with LASSO regularization technique
# Uses builtin sklearn model but with our function to compute p values
def multiple_lasso_regression(X,Y, penalty=0.1):
    X, Y = (X - np.mean(X)) / np.std(X), (Y - np.mean(Y)) / np.std(Y)
    model = Lasso(alpha=penalty)
    model.fit(X, Y)
    Beta = model.coef_
    return (Beta, (len(X[0])-1) * np.array([compute_P(X, Y, Beta, i) for i in range(len(Beta))]))

# Computes Multiple linear regression with Ridge regularization technique
def multiple_ridge_regression(X, Y, penalize=100):
    X, Y = (X - np.mean(X)) / np.std(X), (Y - np.mean(Y)) / np.std(Y)
    X = np.c_[np.ones(len(X)), X]
    Beta = inv(X.T @ X + (penalize * np.identity(len(X.T)))) @ X.T @ Y
    return (Beta, (len(X[0])-1) * np.array([compute_P(X, Y, Beta, i) for i in range(len(Beta))]))

# Computes Multiple linear regression without regularization
def multiple_linear_regression(X, Y):
    X, Y = (X - np.mean(X)) / np.std(X), (Y - np.mean(Y)) / np.std(Y)
    X = np.c_[np.ones(len(X)), X]
    Beta = inv(X.T @ X) @ X.T @ Y
    return (Beta, (len(X[0])-1) * np.array([compute_P(X, Y, Beta, i) for i in range(len(Beta))]))

# Used to test if date is in valid format for mapping within RDD
def try_convert_date(x):
    try:
        y = x[1]['publication_date'][:4] + '-' + str(((int(x[1]['publication_date'][4:6]) - 1)//3))
        return True
    except ValueError:
        return False

# Extracts (X,Y) vectors to feed to regression function
def vectorize(record):
    # record := (patent type, ([list of patent quarter frequency], [list of quarterly gdp]))
    key = record[0]
    patent_quarter_frequency = record[1][0]
    quarterly_gdp = record[1][1]
    N, M = len(quarterly_gdp), len(patent_quarter_frequency)
    pat_qtr_freq_vec = np.zeros((N,))
    qtr_gdp_vec = np.zeros((N,))
    j = 0
    for i in range(N):
        if j < M and patent_quarter_frequency[j][0] == quarterly_gdp[i][0]:
            pat_qtr_freq_vec[i] = float(patent_quarter_frequency[j][1])
            j += 1
        qtr_gdp_vec[i] = float(quarterly_gdp[i][1])
    return (key, pat_qtr_freq_vec, qtr_gdp_vec)

# Performs reduction on data to compute linear regression
def compute_reg(data_rdd, gdp_rdd, regression_attribute, quarterly=True, format=True, regression_method='linear', penalty=100., alpha=0.05):
    threshold = sc.broadcast(alpha)
    # This will be the data attribute for X in multiple_linear_regression(X,Y)
    regression_attribute_name = regression_attribute
    regression_attribute = sc.broadcast(regression_attribute)
    if quarterly:
        data_mapping = lambda x: (x[1]['date'][:4] + '-' + str(((int(x[1]['date'][5:7]) - 1)//3)), [x[1][regression_attribute.value]])
        if format:
            gdp_mapping = lambda x: (x[0][:5] + str(((int(x[0][5:7]) - 1)//3)), x[1])
        else:
            gdp_mapping = lambda x:(x[0], x[1])
    else:
        data_mapping = lambda x: (x[1]['date'][:4], [x[1][regression_attribute.value]])
        gdp_mapping = lambda x: (x[0][:5], x[1])
    # Splits RDD to (quarter, [list of patent quarter frequency based on selected attribute])
    patents_counts_based_on_type_per_quarter = data_rdd.map(lambda x: (x[0], json.loads(x[1]))) \
        .filter(lambda x: 'date' in x[1]) \
        .map(data_mapping) \
        .reduceByKey(lambda x,y: x+y) \
        .map(lambda x: (x[0], np.unique(np.array(x[1]), return_counts=True))) \
        .flatMap(lambda x: [(x[1][0][i], [(x[0], x[1][1][i])]) for i in range(len(x[1][0]))]) \
        .reduceByKey(lambda x,y: x+y) \
        .filter(lambda x: x[0].upper() != 'NULL' and x[0] != '')
    # Get distinct attribute categories
    key_list = patents_counts_based_on_type_per_quarter.map(lambda x: x[0]).distinct().collect()
    key_broadcast = sc.broadcast(key_list)
    # Aggregate quarterly GDB data and produce list of records for each attribute category
    gdp_mapped_rdd = gdp_rdd.map(gdp_mapping) \
        .flatMap(lambda x: [(patent_type, [x]) for patent_type in key_broadcast.value]) \
        .reduceByKey(lambda x,y: x+y)
    # Join RDDs on year and then convert to (X,Y) matrices 
    vectorized_rdd = patents_counts_based_on_type_per_quarter.join(gdp_mapped_rdd) \
        .map(lambda x: (x[0], (sorted(x[1][0], key=lambda y:y[0]), sorted(x[1][1], key=lambda y:y[0])))) \
        .map(vectorize)
    # Compute regression
    if regression_method == 'ridge':
        linear_reg_rdd = vectorized_rdd.map(lambda x: (x[0], multiple_ridge_regression(x[1], x[2], penalty)))
    elif regression_method == 'lasso':
        linear_reg_rdd = vectorized_rdd.map(lambda x: (x[0], multiple_lasso_regression(x[1], x[2], penalty)))
    else:
        linear_reg_rdd = vectorized_rdd.map(lambda x: (x[0], multiple_linear_regression(x[1], x[2])))
    linear_reg_rdd = linear_reg_rdd.filter(lambda x: x[1][1][1] < threshold.value)
    result = sorted(linear_reg_rdd.collect(), key=lambda x: x[0])
    vector_list = sorted(vectorized_rdd.collect(), key=lambda x: x[0])
    np.save(regression_attribute_name, [np.array([vector[1], vector[2]]) for vector in vector_list])
    np.save(regression_attribute_name+'_labels', np.array([vector[0] for vector in result]))
    np.save(regression_attribute_name+'_betas', np.array([vector[1] for vector in result]))
    return result

# Performs reduction on data to compute linear regression
def compute_reg_2(data_rdd, gdp_rdd, attribute_category, regression_attribute, regression_method='linear', penalty=100.):
    # This will be the data attribute for X in multiple_linear_regression(X,Y)
    regression_attribute_name = attribute_category + regression_attribute
    regression_attribute = sc.broadcast(regression_attribute)
    # Splits RDD to (quarter, [list of patent quarter frequency based on selected attribute])
    patents_counts_based_on_type_per_quarter = data_rdd.map(lambda x: (x[0], json.loads(x[1]))) \
        .filter(lambda x: x[1]['country_code'] == 'US') \
        .filter(lambda x: 'publication_date' in x[1]) \
        .filter(lambda x: len(x[1][attribute_category]) > 0) \
        .filter(try_convert_date) \
        .map(lambda x: (x[1]['publication_date'][:4] + '-' + str(((int(x[1]['publication_date'][4:6]) - 1)//3)), [x[1][attribute_category][0][regression_attribute.value]])) \
        .reduceByKey(lambda x,y: x+y) \
        .map(lambda x: (x[0], np.unique(np.array(x[1]), return_counts=True))) \
        .flatMap(lambda x: [(x[1][0][i], [(x[0], x[1][1][i])]) for i in range(len(x[1][0]))]) \
        .reduceByKey(lambda x,y: x+y) \
        .filter(lambda x: x[0] != 'NULL') 
    # Get distinct attribute categories
    key_list = patents_counts_based_on_type_per_quarter.map(lambda x: x[0]).distinct().collect()
    key_broadcast = sc.broadcast(key_list)
    # Aggregate quarterly GDB data and produce list of records for each attribute category
    gdp_mapped_rdd = gdp_rdd.map(lambda x: (x[0][:5] + str(((int(x[0][5:7]) - 1)//3)), x[1])) \
        .flatMap(lambda x: [(patent_type, [x]) for patent_type in key_broadcast.value]) \
        .reduceByKey(lambda x,y: x+y)
    # Join RDDs on year and then convert to (X,Y) matrices 
    vectorized_rdd = patents_counts_based_on_type_per_quarter.join(gdp_mapped_rdd) \
        .map(lambda x: (x[0], (sorted(x[1][0], key=lambda y:y[0]), sorted(x[1][1], key=lambda y:y[0])))) \
        .map(vectorize)
    # Compute regression
    if regression_method == 'ridge':
        linear_reg_rdd = vectorized_rdd.map(lambda x: (x[0], multiple_ridge_regression(x[1], x[2], penalty)))
    elif regression_method == 'lasso':
        linear_reg_rdd = vectorized_rdd.map(lambda x: (x[0], multiple_lasso_regression(x[1], x[2], penalty)))
    else:
        linear_reg_rdd = vectorized_rdd.map(lambda x: (x[0], multiple_linear_regression(x[1], x[2])))
    linear_reg_rdd = linear_reg_rdd.filter(lambda x: x[1][1][1] < .05)
    result = sorted(linear_reg_rdd.collect(), key=lambda x: x[0])
    vector_list = sorted(vectorized_rdd.collect(), key=lambda x: x[0])
    np.save(regression_attribute_name, [np.array([vector[1], vector[2]]) for vector in vector_list])
    np.save(regression_attribute_name+'_labels', np.array([vector[0] for vector in result]))
    np.save(regression_attribute_name+'_betas', np.array([vector[1] for vector in result]))
    return linear_reg_rdd.collect()

# Compute regression on tfidf
def compute_reg_3(year_range=range(1980, 2018), word_limit=8, alpha=.05):
    HDFS_PATH = 'hdfs:///user/kchiguichon/'
    dictionary = set([])
    # Compute word dictionary over all years
    for year in year_range:
        try:
            # Load numpy file from hadoop (HDFS)
            tfidf_file = np.load(io.BytesIO(sc.binaryFiles(HDFS_PATH + str(year) + '_tfidf.npz').take(1)[0][1]))
        except:
            continue
        arrays = sorted(tfidf_file.files)
        year_words = tfidf_file[arrays[0]]
        year_word_rankings = tfidf_file[arrays[1]]
        sorted_indices = np.argsort(year_word_rankings)[::-1]
        dictionary |= set(year_words[sorted_indices][:word_limit].tolist())
    dictionary -= set(['null', 'NULL'] + [str(chr(x)) for x in range(ord('A'), ord('Z'))] + [str(chr(x)) for x in range(ord('a'), ord('z'))])
    dictionary = sorted(list(dictionary))
    year_vectors = []
    j = 0
    for year in year_range:
        try:
            tfidf_file = np.load(io.BytesIO(sc.binaryFiles(HDFS_PATH + str(year) + '_tfidf.npz').take(1)[0][1]))
        except:
            continue
        arrays = sorted(tfidf_file.files)
        year_words = tfidf_file[arrays[0]]
        year_word_rankings = tfidf_file[arrays[1]]
        sorted_indices = np.argsort(year_words)
        year_words = year_words[sorted_indices]
        year_word_rankings = year_word_rankings[sorted_indices]
        year_vectors.append(np.zeros(len(dictionary)))
        for i in range(len(dictionary)):
            index = np.where(year_words == dictionary[i])[0]
            if len(index) > 0:
                year_vectors[j][i] = float(year_word_rankings[index[0]])
        j += 1
    year_vectors = np.array(year_vectors)
    # Setup GDP data
    GDP_PATH = 'hdfs:///user/kchiguichon/GDP.csv'
    gdp_rdd = sc.textFile(GDP_PATH).mapPartitions(lambda x: csv.reader(x))
    gdp_header = gdp_rdd.first()
    gdp_rdd = gdp_rdd.filter(lambda x: x != gdp_header)
    gdp_mapped_rdd = gdp_rdd.map(lambda x: (x[0][:4], x[1]))
    min_range = sc.broadcast(min(year_range))
    max_range = sc.broadcast(max(year_range))
    gdp_data = gdp_mapped_rdd.sortByKey().filter(lambda x: int(x[0]) >= min_range.value and  int(x[0]) <= max_range.value).map(lambda x: (x[0], float(x[1]))).reduceByKey(lambda x,y: x+y).sortByKey().collect()
    gdp_data = np.array([x[1] for x in gdp_data])
    # Compute regression
    Betas, pValues = multiple_linear_regression(year_vectors, gdp_data)
    acceptable_hypothesis = []
    for i in range(1, len(Betas)):
        # Accept only hypothesis meeting threshold
        if pValues[i] < alpha:
            acceptable_hypothesis.append((dictionary[i-1], Betas[i], pValues[i]))
    print(acceptable_hypothesis)

# Retrieves WIPO data for technological patents which specifically characterizes patent as field of science/technology it
# belongs to.
def patent_to_wipo(ydata, regression_type="linear", penalization=100, alpha=0.05):
    # Load row values by using json.loads to convert from json string to python dict.
    mapped_wipo_patents_data = wipo_category_data.map(lambda x: json.loads(x[1]))
    mapped_wipo_category_data = wipo_patents_data.map(lambda x: json.loads(x[1]))
    mapped_patent_data = patent_data.map(lambda x: json.loads(x[1]))
    # Join together wipo_patent which has patent id, field id with wipo_catagory which has field_id, field name
    category_data = mapped_wipo_category_data.map(lambda x: (x["id"],x["field_title"]))
    mapped_wipo_patents_data = mapped_wipo_patents_data.map(lambda x: (x["field_id"], x["patent_id"]))
    joined_wipo_date = category_data.join(mapped_wipo_patents_data).map(lambda x: x[1]).map(lambda x: (x[1], x[0]))
    mapped_patent_data = mapped_patent_data.map(lambda x: (x["id"], x["date"]))
    # Join patent data to associate patents with their field, dump back to json to compute linear regression on.
    full_patent_data = mapped_patent_data.join(joined_wipo_date).map(lambda x: x[1]).map(lambda x: (1,json.dumps({"date": x[0], "field_title": x[1]})))
    return compute_reg(full_patent_data, ydata, "field_title",False,regression_type,penalization,alpha)

# For quarterly data, we care only about data in months of Jan, Apr, Jul, Oct. This function
# parses those months and retrieves the unemployment data for those months, originally, Unemployment.csv contains monthly data.
def map_to_quarters(rdd):
    split_rdd = rdd.strip().split(",")
    ret_list = []
    for i in range(len(split_rdd)):
        if i == 0:
            continue
        elif i == 1:
            ret_list.append((split_rdd[0]+"-0", split_rdd[i]))
        elif i == 4:
            ret_list.append((split_rdd[0] + "-1", split_rdd[i]))
        elif i == 7:
            ret_list.append((split_rdd[0] + "-2", split_rdd[i]))
        elif i == 10:
            ret_list.append((split_rdd[0] + "-3", split_rdd[i]))
    return ret_list

# Retrieves quarterly data of US unemployment data from 1948 to 2018
# returns rdd with format (Year-Quarter, Unemployment rate).
def parse_unemployment_data(sc):
    filepath = "hdfs:///user/jonapi10/Unemployment.csv"
    unem_rdd = sc.textFile(filepath)
    first_record = unem_rdd.first()
    unem_rdd = unem_rdd.filter(lambda x: x != first_record).flatMap(lambda x: map_to_quarters(x))
    return unem_rdd

#Queries BigQuery to get the following rdd format (year (quarter if true), (avg figs, avg sheets, total number of patents))
def join_patents_and_figures(sc,  quarterly=False):
    mapped_patent_data = patent_data.map(lambda rdd: json.loads(rdd[1])).map(lambda rdd:(rdd["id"],(rdd["date"])))
    mapped_figures_data = figures_data.map(lambda rdd: json.loads(rdd[1])).map(lambda rdd: (rdd["patent_id"],
                                                                                     (rdd["num_figures"], rdd["num_sheets"])))
    if quarterly:
        joined_data = mapped_patent_data.join(mapped_figures_data).map(lambda combined: (combined[1][0],combined[1][1][0], combined[1][1][1])).map(
            lambda rdd: (date_to_quarter(rdd[0]), (rdd[1], rdd[2], 1))
        ).reduceByKey(lambda v1, v2: (int(v1[0]) + int(v2[0]), int(v1[1]) + int(v2[1]), int(v1[2]) + int(v2[2]))).map(lambda rdd: (rdd[0], (rdd[1][0]/rdd[1][2], rdd[1][1]/rdd[1][2], rdd[1][2])))
    else:
        joined_data = mapped_patent_data.join(mapped_figures_data).map(
            lambda combined: (combined[1][0], combined[1][1][0], combined[1][1][1])).map(
            lambda rdd: (rdd[0][0:4], (rdd[1], rdd[2], 1))
        ).reduceByKey(lambda v1, v2: (int(v1[0]) + int(v2[0]), int(v1[1]) + int(v2[1]), int(v1[2]) + int(v2[2]))).map(
            lambda rdd: (rdd[0], (rdd[1][0] / rdd[1][2], rdd[1][1] / rdd[1][2], rdd[1][2])))
    return joined_data

def label_list(x, p, alpha):
    new_x = []
    new_p = []
    if p[1] < alpha:
        new_x.append(("Average Patent Fig Count", x[1]))
        new_p.append(p[1])
    if p[2] < alpha:
        new_x.append(("Average Patent Sheet Count", x[2]))
        new_p.append(p[2])
    if p[3] < alpha:
        new_x.append(("Total patents filed", x[3]))
    return (new_x, new_p)

# Does the following, combines data from patent data rdd (year, (avg figs, avg pages, freq)) and some y data (year, value) and feeds it into
# Linear Regression
def linear_regression_on_pat_freq(sc, ydata ,regression_type="linear", penalization=100, quarterly=True, alpha=0.05):
    threshold = sc.broadcast(alpha)
    figures_frequency = join_patents_and_figures(sc, quarterly)
    joined_rdd = figures_frequency.join(ydata).map(lambda rdd: ("A", (rdd[0], rdd[1]))).groupByKey().map(
        lambda rdd: list(rdd[1])).map(
        lambda rdd: (np.array([item[1][0] for item in rdd]).astype(np.float),
                     np.array([item[1][1] for item in rdd]).astype(np.float)))
    if regression_type == "ridge":
        joined_rdd = joined_rdd.map(lambda x: multiple_ridge_regression(x[0], x[1], penalization))
    elif regression_type == "lasso":
        joined_rdd = joined_rdd.map(lambda x: multiple_lasso_regression(x[0], x[1], penalization))
    else:
        joined_rdd = joined_rdd.map(lambda x: multiple_linear_regression(x[0], x[1]))
    joined_rdd = joined_rdd.map(lambda x: (label_list(x[0], x[1], threshold.value)))
    return joined_rdd.collect()

def date_to_quarter(date):
    to_process = date[0:5]
    quarter = int(date[5:7])
    return to_process + str((quarter - 1)//3)

# Map to WDI_indicator_(name, (year, data))
def expand_year_and_name(years, wdi_rdd):
    res = []
    key_name = wdi_rdd[2]
    year_data = wdi_rdd[4:-1]
    for i in range(len(years)):
        if (year_data[i] != ""):
            res.append((key_name, (years[i], year_data[i])))
    return res

# Processes WDI data, specifically interested in USA world development indicators
def parse_wdi_data(sc):
    filepath = "hdfs:///user/jonapi10/WDIData.csv"
    rdd = sc.textFile(filepath).mapPartitions(lambda x: csv.reader(x))
    header = rdd.first()
    # Years header 1960-2018
    years = sc.broadcast(header[4:-1])
    # Filter for USA rows
    usa_rdd = rdd.filter(lambda x: x != header).filter(lambda x: x[1] == "USA")
    # Pick rows with at least 50% values filled (data for 29 different years)
    mapped_rdd = usa_rdd.filter(lambda x: len(["C" for i in x if i != ""]) - 4 >= (int(years.value[-1]) - int (years.value[0])) // 2)
    mapped_rdd = mapped_rdd.flatMap(lambda x: expand_year_and_name(years.value, x))
    # Returns an rdd with format (WDI_name, (year, value))
    return mapped_rdd

# Writes a file of wdi indicator names
def write_keys(rdd):
    keys = rdd.distinct().collect()
    with open("WDIkeys.txt", "w") as file:
        formatted_keys = [key + "\n" for key in keys]
        file.writelines(formatted_keys)

# Helps process data from parse_wdi_data, supplying a WDI name will return a stream of (year, value) rdd.
def getYFromIndicator(rdd, indicator_name):
    selected_rdd = rdd.filter(lambda x: x[0] == indicator_name).map(lambda x: (x[1][0], x[1][1]))
    return selected_rdd

# Some sample queries:
# compute_reg(patent_data, gdp_rdd, 'type')
# compute_reg(foreign_citations_data, gdp_rdd, 'country')
# compute_reg_2(google_patents_publications_data, gdp_rdd, 'citation', 'type')

# google_patents_publications_data.map(lambda x: (x[0], json.loads(x[1]))) \
#     .filter(lambda x: x[1]['country_code'] == 'US') \
#     .filter(lambda x: 'publication_date' in x[1]) \
#     .filter(lambda x: len(x[1]['uspc']) > 0) \
#     .take(1)

# Queries using WDI data
# wdi_data = parse_wdi_data(sc)

# Full indicators can be found in WDI_keys.txt
# ydata = getYFromIndicator(wdi_data, "Population density (people per sq. km of land area)")
# print(compute_reg(patent_data, ydata, "type", quarterly=False))

# Unemployment data
# unemploy_rdd = parse_unemployment_data(sc)
# print(compute_reg(foreign_citations_data, unemploy_rdd, 'country', quarterly=True, format=False))

# Example using Patent size features
# compute_reg(patent_data, gdp_rdd, "")