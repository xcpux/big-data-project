import csv
from pyspark import SparkContext


def map_to_quarters(rdd):
    split_rdd = rdd.strip().split(",")
    ret_list = []
    for i in range(len(split_rdd)):
        if i == 0:
            continue
        elif i == 1:
            ret_list.append((split_rdd[0]+"-0", split_rdd[i]))
        elif i == 4:
            ret_list.append((split_rdd[0] + "-1", split_rdd[i]))
        elif i == 7:
            ret_list.append((split_rdd[0] + "-2", split_rdd[i]))
        elif i == 10:
            ret_list.append((split_rdd[0] + "-3", split_rdd[i]))
    return ret_list


def parse_unemployment_data(sc, filepath):
    unem_rdd = sc.textFile(filepath)
    first_record = unem_rdd.first()
    unem_rdd = unem_rdd.filter(lambda x: x != first_record).flatMap(lambda x: map_to_quarters(x))
    return unem_rdd

#sc = SparkContext.getOrCreate()
#print(parse_unemployment_data(sc , "../data/Unemployment.csv").collect())