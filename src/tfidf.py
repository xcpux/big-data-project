### 
#   Group Members: 
#       Kenneth Chiguichon, Darwin Shen, Jonathan Yin
# 
#   Code Description:  
#       Performs tfidf on the abstracts of the patent data for each year.
#
#    Data Frameworks:
#       1) Data was taken from BigQuery,
#       2) Spark was used to perform the tfidf calculations.
#       3) The final results were saved to HDFS to be used later.
#   
#   Class Concepts:
#       1) Implements tfidf.
#
#   System Specs:
#       OS: Ubuntu
#       IAAS Provider: Google Cloud (DataProc)
#       Nodes: 5+1 (workers + master)
#       CPU(s) per Node: 4
#       RAM per Node: 16GB
#       Storage per Node: 100GB (HDD)
#
###


from pprint import pprint
import csv
import json
import numpy as np
from pyspark import SparkContext
import math
from nltk.corpus import stopwords
import nltk
import re
from subprocess import PIPE, Popen

# sorts words by tfidf score and return x top values (x is currently set to 20)  
def process(x):
    temp = x[1]
    sortedResults = sorted(temp, key = lambda tup: tup[0], reverse = True)
    return sortedResults[:20]


if __name__ == "__main__":
    sc = SparkContext()
    # config for patent data from big query
    conf = {
        'mapred.bq.input.project.id': 'patents-public-data',
        'mapred.bq.input.dataset.id': 'patentsview',
        'mapred.bq.input.table.id': 'patent'
    }

    patent_data = sc.newAPIHadoopRDD(
        'com.google.cloud.hadoop.io.bigquery.JsonTextBigQueryInputFormat',
        'org.apache.hadoop.io.LongWritable',
        'com.google.gson.JsonObject',
        conf=conf
    )

    # use stopwords from nltk
    nltk.download('stopwords')
    stop_words = set(stopwords.words('english'))
    bcStopwords = sc.broadcast(stop_words)

    # remove punctation and make all lowercase
    def lower_clean_str(x):
        punc='!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
        lowercased_str = x.lower()
        for ch in punc:
            lowercased_str = lowercased_str.replace(ch, ' ')
        return lowercased_str

    # uses regex to remove words with numbers in it
    # splits the abstract so that each word is a row with id
    # removes stopwords 
    def explode(row):
        pattern = re.compile(r'[A-Za-z]*[0-9]+[A-Za-z]*')
        output = []
        words = row[2].split()
        words = [word for word in words if (word not in bcStopwords.value and  not re.match(pattern, word))]
        for word in words:
            output.append(((word,row[0], row[1]), 1))
        return output


    # runs tfidf on the abstracts of the patents per year
    # returns the top 20 words per document and aggregates a count of top words across all documents in a year and saves the result in as .npz files 
    # default year range is 1980 up to 2018. 
    def tf_idf(yearRange = range(1980,2018)):
        for year in [str(year) for year in yearRange]:
            # loads data from BigQuery rdd; abstract must exist
            dates = patent_data.map(lambda rdd: json.loads(rdd[1])).filter(lambda x: 'abstract' in x).map(lambda rdd:(rdd["id"],(rdd["date"][:4]),lower_clean_str(rdd["abstract"])))
            wantedYear = sc.broadcast(year)
            # filters to only current year
            dates = dates.filter(lambda x: x[1] == wantedYear.value)
            # counts number of documents
            documents = dates.count() 
            exploded = dates.flatMap(lambda x: explode(x))
            termF = exploded.reduceByKey(lambda x,y: x+y) #((word, doc id, year), number of time word appears in document)
            explodedDocs = termF.map(lambda x: ((x[0][0], x[0][2]), 1))
            docF = explodedDocs.reduceByKey(lambda x,y: x+y) # (word, number of documents it appears in)
            # idf calculation: log((|D| + 1) / (DF + 1))
            idf = docF.map(lambda x: (x[0], x[1], math.log(documents + 1) / (x[1] + 1)))
            termF = termF.map(lambda x: ((x[0][0], x[0][2]), (x[0][1], x[1])))
            idf = idf.map(lambda x: (x[0], (x[1],x[2])))
            tfidf = termF.leftOuterJoin(idf)
            tfidf = tfidf.map(lambda x: ((x[0][0],x[1][0][0],x[0][1]),(x[1][0][1]*x[1][1][1])))  #((word, doc id, year), tf*idf)
            tfidfYear = tfidf.map(lambda x: ((x[0][2], x[0][1]), [(x[1],x[0][0])]))
            tfidfYear = tfidfYear.reduceByKey(lambda x,y: x+y)
            
            test = tfidfYear.map(lambda x: (x[0][0], process(x)))
            test = test.reduceByKey(lambda x,y: x+y)
            test = test.map(lambda x: (x[0], np.unique(np.array([term[1] for term in x[1]]), return_counts = True)))
            # save .npz file and put into hdfs
            output_path = year+'_tfidf'
            np.savez(output_path, test.collect()[0][1][0], test.collect()[0][1][1])
            hdfs_process = Popen(["hadoop", "fs", "-put", output_path + '.npz'], stdin=PIPE, bufsize=-1)
            hdfs_process.communicate()
            print('Saved and uploaded year '+year+' to hdfs.')
            
    tf_idf()
