from sklearn.preprocessing import StandardScaler
import numpy as np
import pyspark
from scipy import stats

def perform_linear_regression(rdd):
    #(groupnorm, income, heart_disease)
    scaler = StandardScaler()
    data = rdd[1]
    full_data = np.array([[item[0], item[1], item[2]] for item in data])
    scaler.fit(full_data)
    standardized_data = scaler.transform(full_data)
    standardized_with_constant = np.c_[np.ones(len(standardized_data[:,0])), standardized_data]
    X_nocontrol = standardized_with_constant[:,[0,1]]
    X_control = standardized_with_constant[:,[0,1,2]]
    Y = standardized_with_constant[:,3]
    pval_simple, beta_simple = simple_linear_regression(X_nocontrol, Y)
    pval, beta_multi = multivariate_linear_regression(X_control, Y)
    return (rdd[0], (pval_simple,beta_simple), (pval, beta_multi))

def multivariate_linear_regression(X,Y):
    Beta = np.matmul(np.matmul(np.linalg.inv(np.matmul(X.T, X)), X.T), Y)
    pval = compute_P(len(X), 2, X, Y, Beta, 1)
    return (pval, Beta[1])

def simple_linear_regression(X, Y):
    Beta = np.matmul(np.matmul(np.linalg.inv(np.matmul(X.T, X)), X.T), Y)
    pval = compute_P(len(X), 1, X, Y, Beta, 1)
    return (pval, Beta[1])


def compute_P(num_observations, num_features, X, Y, Beta, beta_index):
    degrees_freedom = num_observations - (num_features + 1)
    Y_predicted = np.matmul(X, Beta)
    RSS = np.sum((Y - Y_predicted)**2)
    X_col = X[:,beta_index]
    std_err = RSS / degrees_freedom
    denom = np.sum((X_col - X_col.mean())**2)
    std_err = np.sqrt(std_err/denom)
    t = Beta[beta_index]/std_err
    pval = 1 - stats.t.cdf(t, df=degrees_freedom)
    return pval * 2
